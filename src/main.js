import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import BootstrapVueNext from "bootstrap-vue-next";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue-next/dist/bootstrap-vue-next.css";

import "swiper/css";
import "swiper/css/bundle";

// import "./assets/custom.css";

import "./assets/css/boxicons.min.css";
import "./assets/fonts/flaticon.css";
import "./assets/css/meanmenu.css";
import "./assets/css/style.css";
import "./assets/css/responsive.css";
import "./assets/css/theme-dark.css";

const app = createApp(App).use(router);
app.use(BootstrapVueNext);
app.mount("#app");
